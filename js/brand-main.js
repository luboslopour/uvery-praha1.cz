/* js library */
// custom object to store global variables within window namespace

var shareData = {
    url: location.protocol + "//" + location.host + "/"
};

// loan form type default
shareData.loanFormType = 0;

jQuery(document).ready(function ($) { 


    // mobile menu
    $('.mainmenu').prepend('<a href="" class="menu_toggle">Menu</a>');
    $('.mainmenu .menu_toggle').on('click',function(e){
        e.preventDefault();
        $(this).parents('nav').toggleClass('on');
    });


    // loan form slider config
    shareData.loanFormSliderConfig = [
    {
        text: 'RPSN činí [rpsn] %, celková částka, kterou je potřeba zaplatit, činí [creditTotal] Kč. Roční úroková sazba je [annualReport]\xA0%.',
        amount: {
            valueMin: parseInt($('#0-amountMin').val()),
            valueMax: parseInt($('#0-amountMax').val()),
            valueStep: parseInt($('#0-amountStep').val())
        },
        maturity: {
            valueMin: parseInt($('#0-maturityMin').val()),
            valueMax: parseInt($('#0-maturityMax').val()),
            valueStep: parseInt($('#0-maturityStep').val())
        }
    },
    {
        text: 'Celková částka, kterou je potřeba zaplatit, činí [creditTotal] Kč.',
        amount: {
            valueMin: parseInt($('#1-amountMin').val()),
            valueMax: parseInt($('#1-amountMax').val()),
            valueStep: parseInt($('#1-amountStep').val())
        },
        maturity: {
            valueMin: parseInt($('#1-maturityMin').val()),
            valueMax: parseInt($('#1-maturityMax').val()),
            valueStep: parseInt($('#1-maturityStep').val())
        }
    }
];
    // hezke inputy ve formularich (laon)
    if ($('div.form.loan-form').length) {
        //---if sended
        if($('#r-type').length && $('#r-type').val() != -1) {
            var rtype = $('#r-type').val();
        } else {
            var rtype = 0;
        }
        $('#r-type').val(-1);
        //---end if sended 
        setSliderLoanForm(shareData.loanFormSliderConfig[rtype]);
        if (jQuery.isFunction(init)) {
            init();
        }

        $('div.form.loan-form input[type=checkbox]').checkBox();
        $('div.form.loan-form input[type=radio]').checkBox();
        
        //$('div.form.loan-form input[type=radio]').click(function () 
        $('#loan-form-id1, #loan-form-id2').click(function () {
            $('fieldset.slider.amount div.slider').slider('destroy');
            $('fieldset.slider.maturity div.slider').slider('destroy');

            // set to global variable
            shareData.loanFormType = $(this).val();

            // set slider
            setSliderLoanForm(shareData.loanFormSliderConfig[shareData.loanFormType]);
 
        });
    }
    
    // hezke inputy ve formularich (contact)
    if ($('div.form.contact-form').length) {
        
        $('div.form.contact-form input[type=checkbox]').checkBox();
        $('div.form.contact-form input[type=radio]').checkBox();
        
        $('div.form.contact-form select').sSelect();
        $('div.form.contact-form div.newListSelected').append('<div class="dropdown"><span class="ico"></span></div>');

        $('div.form.contact-form div.dropdown').click(function() {
            $(this).parents('div.newListSelected').find('div.SSContainerDivWrapper').toggle();
        });

        $('div.newListSelected').click(function() {
            if ($(this).find('div.selectedTxt').text() != 'Vyberte jednu z možností') {
                $(this).css('color', 'rgb(51,51,51)');
            } else {
                $(this).css('color', 'rgb(153,153,153)');
            }
        }); 
    }
    
    if ($('div#click-to-loan').length) {
        $('div#click-to-loan input[type=checkbox]').checkBox();
    }
    
    // Open links in new window
    $("a[rel='external']").click(function() {
        window.open(this.href);
        return false;
    });

    /*$('div.dropdown').click(function () {
        $(this).parents('div.newListSelected').find('div.SSContainerDivWrapper').toggle();
    });*/

    $('div.newListSelected').click(function () {
        if ($(this).find('div.selectedTxt').text() != 'Vyberte jednu z možností')
            $(this).css('color', 'rgb(0,0,0)');
        else
            $(this).css('color', 'rgb(102,102,102)');
    });

    // obsluhuje textova pole/textarey pro default text
    $('form input[type=text], form textarea').click(function () {
        if ($(this).attr("title") != undefined) {
            if ($(this).val() == $(this).attr("title"))
                $(this).val('');
        }
    });
    $('form input[type=text], form textarea').blur(function () {
        if ($(this).attr("title") != undefined) {
            if ($(this).val() == '')
                $(this).val($(this).attr("title"));
        }
    });

    // nastavuje/obsluhuje modaly
    $('#mail').dialog({
        autoOpen: false,
        height: 'auto',
        width: 500,
        modal: true
    });
    
    //nastavuje banner
    $('.wideSquare').dialog({
        autoOpen: true,
        height: 300,
        width: 300,
        modal: true
    });

    $('li.mail a').click(function () {
        $('#mail').dialog('open');
        return false;
    });

    // obsluhuje modaly
    $('button.ui-dialog-titlebar-close').attr('title', 'Zavřít okno');


    // obsluhuje galerii v detailu clanku
    $('div.gallery a').each(function (index) {
        $(this).attr('rel', 'gallery');
    });

    $('div.gallery a').fancybox({
        padding: 20,
        overlayOpacity: 0.75,
        overlayColor: 'rgb(0,0,0)',
        titlePosition: 'inside',
        titleFormat: function (title, currentArray, currentIndex, currentOpts) {
            return '<samp>' + (currentIndex + 1) + ' / ' + currentArray.length + '</samp> ' + title;
        }
    });

    // obsluhuje fancybox
    $('a#fancybox-close').append('<span class="ico"></span>');
    $('a#fancybox-left').attr('title', 'Předchozí');
    $('a#fancybox-right').attr('title', 'Další');
    $('a#fancybox-close').attr('title', 'Zavřít');

    // antispam
    if ($('.antispamblock').length) {
        $('.antispam').val('777');
        $('.antispamblock').hide();
    }
    
    // faq
    $('div.faq h2').click(function(){
        if($(this).parent().find('div.answer').is(":visible")){
            $('.answer').hide();
        } else {
            $('.answer').hide();
            $(this).parent().find('.answer').show();
        }
    })


        // obsluhuje/spousti hlavni slider na homepage
    if ($('div.homepage div.main.slider').length) {
        $('div.main.slider div.items').bxSlider({
            mode: 'horizontal',
            slideSelector: '',
            infiniteLoop: true,
            hideControlOnEnd: false,
            speed: 500,
            easing: null,
            slideMargin: 0,
            startSlide: 0,
            randomStart: false,
            captions: false,
            ticker: false,
            tickerHover: false,
            adaptiveHeight: false,
            adaptiveHeightSpeed: 500,
            video: false,
            useCSS: true,
            preloadImages: 'all',
            touchEnabled: true,
            swipeThreshold: 50,
            oneToOneTouch: true,
            preventDefaultSwipeX: true,
            preventDefaultSwipeY: false,
            pager: false,
            pagerType: 'full',
            pagerShortSeparator: ' / ',
            pagerSelector: null,
            buildPager: null,
            pagerCustom: null,
            controls: true,
            nextText: '',
            prevText: '',
            nextSelector: null,
            prevSelector: null,
            autoControls: false,
            startText: 'Start',
            stopText: 'Stop',
            autoControlsCombine: false,
            autoControlsSelector: null,
            auto: false,
            pause: 4000,
            autoStart: true,
            autoDirection: 'next',
            autoHover: false,
            autoDelay: 0,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideWidth: 0
        });
        $('a.bx-next').attr('title', 'Další');
        $('a.bx-prev').attr('title', 'Předchozí');
    }
    
    //obsluhuje hlavni (no-)slider
    var body_width = $('body').width();
    var image_width = $('div.main div.items div.item:first-child div.figure img').width();
    if(body_width < image_width) {
        $('div.main div.items div.item:first-child div.figure img').css('margin-left',(body_width - image_width)/2);
    }
    $(window).resize(function() {
        var body_width = $('body').width();
        var image_width = $('div.main div.items div.item:first-child div.figure img').width();
        if(body_width < image_width) {
            $('div.main div.items div.item:first-child div.figure img').css('margin-left',(body_width - image_width)/2);
        }
        else {
            $('div.main div.items div.item:first-child div.figure img').css('margin-left','auto');
        }
    });
// 17.9.2014
    if ($('a.increase').length) {
        $('a.increase').on('click',function() {
        if ($(this).parent().hasClass('amount')) {
            var slider = $('fieldset.slider.amount div.slider');
            var unit = ' Kč';
        }
        else {
            var slider = $('fieldset.slider.maturity div.slider');
            var unit = ' měsíců';
        }
        slider.slider('value', slider.slider('value') + slider.slider('option','step'));
        slider.parent().find('a.ui-slider-handle').text(slider.slider('value') + unit);
        slider.parent().find('> p:first-child samp').text(slider.slider('value') + unit);
    });   
    }
    
    if ($('a.decrease').length) {
        $('a.decrease').on('click',function() {
        if ($(this).parent().hasClass('amount')) {
            var slider = $('fieldset.slider.amount div.slider');
            var unit = ' Kč';
        }
        else {
            var slider = $('fieldset.slider.maturity div.slider');
            var unit = ' měsíců';
        }
        slider.slider('value', slider.slider('value') - slider.slider('option','step'));
        slider.parent().find('a.ui-slider-handle').text(slider.slider('value') + unit);
        slider.parent().find('> p:first-child samp').text(slider.slider('value') + unit);
    });
    } 
    
    //obsluhuje click-to-loan modal
    $('#click-to-loan').dialog({
        autoOpen: false,
        height: 400,
        width: 600,
        modal: true,
                open: function(){ //close when click outside
                    $('.ui-widget-overlay').bind('click',function(){
                    $('#click-to-loan').dialog('close');
                    })
                }
    });

    $('a.ribbon').click(function() {
        $('#click-to-loan').dialog('open');
        return false;
    });
        
        
        
        if ($('div#click-to-loan').length) {
            if ($('#click-to-loan').data('open-ribbon') == '1') {
                $('#click-to-loan').dialog('open');
            }
            $('#ctl_send').on('click', function() {
                $('#rs-ctl-pixel').remove();
                $('#click-to-loan p.message').hide();
                $.post('/modules/banner/ajax/clickToLoan.php', 
                {'ctl_phone': $('#click-to-loan-id1').val(), 
                 'ctl_agree': $('#click-to-loan-id2').prop('checked')}, 
                function(data) {
                    if (data.result == true) {
                        $('#click-to-loan p.message.success').show();
                        if (data.pixel != '') {
                        $('#click-to-loan').append('<img src="'+data.pixel+'" width="1" height="1" border="0" alt="" id="rs-ctl-pixel">');
                        }
                    } else {
                        $('#click-to-loan p.message.fault').show();   
                    }
                    
                }, 'json'
            );
                return false;
            });
        }
    
});

var loadLoanFormCalculation = function (_input) {
    var type = _input.type;
    
    $.ajax({
        type: 'GET',
        url: shareData.url + 'loanCount.php',
        data: {
            credit: parseInt(_input.credit),
            term: parseInt(_input.term),
            type: parseInt(_input.type)

        },
        success: function (_input) {
            var data = _input.data[0];

            // update big red text
            $('#loan-form-id3').val(data.payment);
                   
            // update sentence 
            var text = shareData.loanFormSliderConfig[type].text;
            var rpsn = data.rpsn.replace('.',',');
            var credit_total = data.credit_total.replace('.',',');
            var annual_report = data.annual_report.replace('.',',');
            
            text = text.replace('[rpsn]', rpsn)
                .replace('[creditTotal]', credit_total)
                .replace('[annualReport]', annual_report);

            $('#loan-form-id3').parents('fieldset').children('p.info').text(text);

            // store in hidden inputs
            $('#hiddenCredit').val(data.credit.replace('.',','));
            $('#hiddenTerm').val(data.term.replace('.',','));
            $('#hiddenPayment').val(data.payment.replace('.',','));
            $('#hiddenCreditTotal').val(data.credit_total.replace('.',','));
            $('#hiddenAnnualReport').val(data.annual_report.replace('.',','));
            $('#hiddenRpsn').val(data.rpsn.replace('.',','));
            $('#hiddenType').val(type);
        },
        dataType: 'json',
        timeout: 60000
    });
};

// loan slider form
var setSliderLoanForm = function (_input) {
    // if no form is present retur
    if (!$('fieldset.slider.amount').length && !$('fieldset.slider.maturity').length) {
        return false;
    }
    
    if (typeof(_input) == 'undefined') {
        _input = shareData.loanFormSliderConfig[0];
    }
    
    // take values from _input config
    var val_min = _input.amount.valueMin;
    var val_max = _input.amount.valueMax;
    var val_step = _input.amount.valueStep;
    //---settings if sended---
        if($('#r-credit').length && $('#r-credit').val() != -1) {
            var rcredit = $('#r-credit').val();
        } else {
            var rcredit = val_min;
        }
        $('#r-credit').val(-1);
    //---end if sended---
    
    // slider amount
    $('fieldset.slider.amount div.slider').slider({
        min: val_min,
        max: val_max,
        value: rcredit,
        step: val_step,
        range: 'min',
        slide: function (event, ui) {
            $('fieldset.slider.amount a.ui-slider-handle').text(ui.value + ' Kč');
            $('fieldset.slider.amount input[name=amount]').val(ui.value);
            $('fieldset.data input#loan-form-id12').val(ui.value + ' Kč');

            loadLoanFormCalculation({
                credit: ui.value,
                term: $('fieldset.slider.maturity div.slider').slider('value'),
                type: shareData.loanFormType
            });
        }
    });
    $('fieldset.slider.amount a.ui-slider-handle').text(val_min + ' Kč');
    $('fieldset.slider.amount div.slider div.ui-slider-range').append('<samp>' + val_min + ' Kč</samp>').append('<samp>' + val_max + ' Kč</samp>');
    $('fieldset.slider.amount div.slider').append('<input name="amount" type="hidden" value=' + val_min + '>');
    $('fieldset.data input#loan-form-id12').val(val_min + ' Kč');
    //if sended
        $('fieldset.slider.amount a.ui-slider-handle').text(rcredit + ' Kč');
        $('fieldset.slider.amount input[name=amount]').val(rcredit);
        $('fieldset.data input#loan-form-id12').val(rcredit + ' Kč');
    //end if sended
    
    // slider maturity
    var val_min = _input.maturity.valueMin;
    var val_max = _input.maturity.valueMax;
    var val_step = _input.maturity.valueStep; 
    //---settings if sended---
        if($('#r-term').length && $('#r-term').val() != -1) {
            var rterm = $('#r-term').val();
            if (jQuery.isFunction(init)) {
                init();
            }
        } else {
            var rterm = val_min;
        }
        $('#r-term').val(-1);
    //---end if sended---
    $('fieldset.slider.maturity div.slider').slider({
        min: val_min,
        max: val_max,
        value: rterm,
        step: val_step,
        range: 'min',
        slide: function (event, ui) {
            $('fieldset.slider.maturity a.ui-slider-handle').text(ui.value + ' měsíců');
            $('fieldset.slider.maturity input[name=maturity]').val(ui.value);
            console.log('maturity - ' + ui.value);
            loadLoanFormCalculation({
                credit: $('fieldset.slider.amount div.slider').slider('value'),
                term: ui.value,
                type: shareData.loanFormType
            });
        }
    });
    $('fieldset.slider.maturity a.ui-slider-handle').text(val_min + ' měsíců');
    $('fieldset.slider.maturity div.slider div.ui-slider-range').append('<samp>' + val_min + ' měsíců</samp>').append('<samp>' + val_max + ' měsíců</samp>');
    $('fieldset.slider.maturity div.slider').append('<input name="maturity" type="hidden" value=' + val_min + '>');

    //---if sended---
            $('fieldset.slider.maturity a.ui-slider-handle').text(rterm + ' měsíců');
            $('fieldset.slider.maturity input[name=maturity]').val(rterm);
    //---end if sended---
    loadLoanFormCalculation({
        credit: $('fieldset.slider.amount div.slider').slider('value'),
        term: $('fieldset.slider.maturity div.slider').slider('value'),
        type: shareData.loanFormType
    });
};

var init = function() {
    var error = $('#demand-form').data('error');
    if (error == 'ok') {
        var crd = shareData.loanFormSliderConfig[0]['amount']['valueMin'];
        var trm = shareData.loanFormSliderConfig[0]['maturity']['valueMin'];
    
        $('fieldset.slider.amount a.ui-slider-handle').text(crd + ' Kč');
        $('fieldset.slider.amount input[name=amount]').val(crd);
        $('fieldset.data input#loan-form-id12').val(crd + ' Kč');
    
        $('fieldset.slider.maturity input[name=maturity]').val(trm);
        $('fieldset.slider.maturity a.ui-slider-handle').text(trm + ' měsíců');
    }
};