/* WWW.???.CZ, JS LIBRARY */


jQuery(document).ready(function($) {

	// hezke selecty/inputy ve formularich
	if ($('form').length) {
		$('form select').sSelect();
		$('form div.newListSelected').append('<div class="dropdown"><span class="ico"></span></div>');
	}

	$('div.dropdown').click(function() {
		$(this).parents('div.newListSelected').find('div.SSContainerDivWrapper').toggle();
	});

	$('div.newListSelected').click(function() {
		if ($(this).find('div.selectedTxt').text() != 'Vyberte jednu z možností')
			$(this).css('color', 'rgb(0,0,0)');
		else
			$(this).css('color', 'rgb(102,102,102)');
	});


	// obsluhuje textova pole/textarey pro default text
	$('form input[type=text], form textarea').click(function() {
		if ($(this).attr("title") != undefined)
		{
			if ($(this).val() == $(this).attr("title"))
				$(this).val('');
		}
	});
	$('form input[type=text], form textarea').blur(function() {
		if ($(this).attr("title") != undefined)
		{
			if ($(this).val() == '')
				$(this).val($(this).attr("title"));
		}
	});


	// nastavuje/obsluhuje modaly
	$('#mail, #searchform, #we-call-back').dialog({
		autoOpen: false,
		height: 'auto',
		width: 500,
		modal: true
	});

	$('li.mail a').click(function() {
		$('#mail').dialog('open');
		return false;
	});
	$('li.search a').click(function() {
		$('#searchform').dialog('open');
		return false;
	});
	$('p.we-call-back a').click(function() {
		$('#we-call-back').dialog('open');
		return false;
	});


	// obsluhuje modaly
	$('button.ui-dialog-titlebar-close').attr('title', 'Zavřít okno');


	// obsluhuje galerii v detailu clanku
	$('div.gallery a').each(function(index) {
		$(this).attr('rel', 'gallery');
	});

	$('div.gallery a').fancybox({
		padding: 20,
		overlayOpacity: 0.75,
		overlayColor: 'rgb(0,0,0)',
		titlePosition: 'inside',
		titleFormat: function(title, currentArray, currentIndex, currentOpts) {
			return '<samp>' + (currentIndex + 1) + ' / ' + currentArray.length + '</samp> ' + title;
		}
	});


	// obsluhuje fancybox
	$('a#fancybox-close').append('<span class="ico"></span>');
	$('a#fancybox-left').attr('title', 'Předchozí');
	$('a#fancybox-right').attr('title', 'Další');
	$('a#fancybox-close').attr('title', 'Zavřít');


	// obsluhuje/spousti hlavni slider na homepage
	if ($('div.homepage div.content div.main.slider').length) {
		var homepage_slider = $('div.content div.main.slider div.items').bxSlider({
			mode: 'horizontal',
			slideSelector: '',
			infiniteLoop: true,
			hideControlOnEnd: false,
			speed: 500,
			easing: null,
			slideMargin: 0,
			startSlide: 0,
			randomStart: false,
			captions: false,
			ticker: false,
			tickerHover: false,
			adaptiveHeight: false,
			adaptiveHeightSpeed: 500,
			video: false,
			useCSS: true,
			preloadImages: 'all',
			touchEnabled: true,
			swipeThreshold: 50,
			oneToOneTouch: true,
			preventDefaultSwipeX: true,
			preventDefaultSwipeY: false,
			pager: false,
			pagerType: 'full',
			pagerShortSeparator: ' / ',
			pagerSelector: null,
			buildPager: null,
			pagerCustom: null,
			controls: true,
			nextText: '&rarr;<span class="ico"></span>',
			prevText: '&larr;<span class="ico"></span>',
			nextSelector: null,
			prevSelector: null,
			autoControls: false,
			startText: 'Start',
			stopText: 'Stop',
			autoControlsCombine: false,
			autoControlsSelector: null,
			auto: false,
			pause: 4000,
			autoStart: true,
			autoDirection: 'next',
			autoHover: false,
			autoDelay: 0,
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideWidth: 0
		});
		$('a.bx-next').attr('title', 'Další');
		$('a.bx-prev').attr('title', 'Předchozí');
	}


	// obsluhuje zalozky a k nim prirazene karty
	if ($('div.bookmarks, div.cards').length) {
		$('div.bookmarks li:first-child').find('a').addClass('active');
		$('div.cards > div').not(':first-child').addClass('inactive');
	}

	$('div.bookmarks a').click(function() {
		$(this).parents('div.bookmarks').find('a').removeClass('active');
		$(this).addClass('active');
		var el = $(this).parents('li').attr("class");
		if ($('div.dealer').length) {
			$(this).parents('div.cards').find('> div').not('.bookmarks').removeClass('active').addClass('inactive');
			$(this).parents('div.cards').find('> div.' + el).removeClass('inactive').addClass('active');
		}
		else {
			$(this).parents('div.bookmarks').find('+ div.cards > div').removeClass('active').addClass('inactive');
			$(this).parents('div.bookmarks').find('+ div.cards > div').each(function(index) {
				if ($(this).hasClass(el)) {
					$(this).removeClass('inactive').addClass('active');
				}
			});
		}
	});


	// doplnuje grafiku blockquote
	$('div.content blockquote').append('<span class="ico"></span>');


	// obsluhuje vyber data
	if ($('input.datepicker').length) {
		$('input.datepicker').datepicker({
			closeText: 'Zavřít',
			prevText: '',
			nextText: '',
			currentText: 'Dnes',
			monthNames: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
			monthNamesShort: ['Le', 'Ún', 'Bř', 'Du', 'Kv', 'Čn', 'Čc', 'Sr', 'Zá', 'Ří', 'Li', 'Pr'],
			dayNames: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
			dayNamesShort: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
			dayNamesMin: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
			weekHeader: 'Sm',
			dateFormat: "dd. mm. yy",
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: '',
			minDate: 0,
			showButtonPanel: false
		});
	}

});
